#include <gtest/gtest.h>

bool f1(){
    return false ;
}

int f2(int a,int b){
    return a+b ;
}

TEST(Test1, bool_Test){
    const bool result1=f1();
    EXPECT_EQ(true, result1) << "KO !";
}

TEST(Test2, addition_Test){
    const int a=5;
    const int b=4;
    const int result2=f2(a,b);
    EXPECT_EQ(10, result2) << "KOO !";
}

